package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random; 


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while(true){
            System.out.println("Let's play round " + roundCounter);
                        
            String humanmove = readInput("Your choice (Rock/Paper/Scissors)?");
            String computermove = getcomputermove(rpsChoices);
            
            while(!humanmove.equals("rock") && !humanmove.equals("paper") && !humanmove.equals("scissors")){
                System.out.println("I do not understand " + humanmove + ". " + "Could you try again?");
                humanmove = readInput("Your choice (Rock/Paper/Scissors)?");
            }

            String winner = " ";

            if(humanmove.equals(computermove)){
                winner = "It's a tie!";
            }
            
            else if(humanmove.equals("rock") && computermove.equals("scissors") || humanmove.equals("paper") && computermove.equals("rock") || humanmove.equals("scissors") && computermove.equals("paper")){
                winner = "Human wins!";
                humanScore ++;
            }
            else{
                winner = "Computer wins!";
                computerScore ++;
            }


            System.out.println("Human chose " + humanmove + ", " + "computer chose " + computermove + ". " + winner);
            System.out.println("Score: human " + humanScore + ", " + "computer " + computerScore);

            roundCounter ++;

            String YesorNo = readInput("Do you wish to continue playing? (y/n)?");

            if(YesorNo.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
        }

    }


    
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String getcomputermove(List<String> array){
        Random randomnumber = new Random();
        int index = randomnumber.nextInt(array.size());
        String randomchoice = array.get(index);
        return randomchoice;
    }

    
}
